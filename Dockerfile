FROM registry.gitlab.com/harish_test_group/hsr_test_k8s/minimal-rails-app/dependencies:latest
# RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
# RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /myapp

# Add a script to be executed every time the container starts.
# COPY entrypoint.sh /usr/bin/
# RUN chmod +x /usr/bin/entrypoint.sh
# ENTRYPOINT ["entrypoint.sh"]
# EXPOSE 3000
EXPOSE 5000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0", "-p", "5000"]
